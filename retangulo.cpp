#include "retangulo.hpp"

retangulo::retangulo()
{
	setBase(15.5);
	setAltura(20);
}

retangulo::retangulo(float base, float altura)
{
	setBase(base);
	setAltura(altura);
}

float retangulo::area (){
	return getBase() * getAltura();
}
