#include "quadrado.hpp"

quadrado::quadrado ()
{
	setBase(21);
}

quadrado::quadrado (float base)
{
	setBase(base);
}

float quadrado::area (){
	return (getBase () * getBase ());
}
