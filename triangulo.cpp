#include "triangulo.hpp"

triangulo::triangulo()
{
	setBase (5.5);
	setAltura (2.5);
}

triangulo::triangulo (float base, float altura)
{
	setBase (base);
	setAltura (altura);
}

float triangulo::area (){
	return (getBase() * getAltura())/2;
}
