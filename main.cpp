#include <iostream>

#include "retangulo.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"

using namespace std;

int main () {
	
	geometrica * formaRetangulo = new retangulo (10,20);	
	geometrica * formaTriangulo = new triangulo (10,10);	
	geometrica * formaQuadrado = new quadrado (10);
	
	cout << "Calculo da Area do Retangulo: " << formaRetangulo->area () << endl;
	cout << "Calculo da Area do Triangulo: " << formaTriangulo->area () << endl;
	cout << "Calculo da Area do Quadrado: " << formaQuadrado->area () << endl;
	
	return 0;
}
