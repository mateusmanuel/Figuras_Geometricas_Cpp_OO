#ifndef GEOMETRICA_H
#define GEOMETRICA_H

class geometrica {

	private:
		float base, altura;
		
	public:
		geometrica ();
		geometrica (float base, float altura);
	
		float getAltura ();
		float getBase ();
		void setAltura (float altura);
		void setBase (float base);
		
		virtual float area () = 0;
		
};

#endif		
		
